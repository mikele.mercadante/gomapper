package main

import (
	"fmt"
	r "goMapper/reflectioninfo"

	_ "github.com/denisenkom/go-mssqldb"
)

func main() {

}

func tryMap(source interface{}, target interface{}) {

	reflectionInfoSource := r.New(source)
	reflectionInfoTarget := r.New(target)

	fmt.Println(reflectionInfoTarget.GetFieldsName())
	fmt.Println(reflectionInfoSource.GetFieldsName())
}
