package reflectioninfo

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

type emptyStruct struct {
}

type fakeStruct struct {
	Field1  int
	Field2  float32
	Field3  bool
	Field4  string
	Field5  rune
	Field6  byte
	Fields7 []complex128
}

func TestGetRefletionInfoInstance(t *testing.T) {

	assert.NotNil(t, New(new(emptyStruct)), "should be not null")
}

func TestGetNameOfPublicField(t *testing.T) {
	expectedFields := [7]string{"Field1", "Field2", "Field3", "Field4", "Field5", "Field6", "Fields7"}

	sut := New(new(fakeStruct))

	assert.Subset(t, expectedFields, sut.GetFieldsName())
}

func TestGetTypeOfPublicFieldByName(t *testing.T) {

	sut := New(new(fakeStruct))

	assert.EqualValues(t, reflect.Int, sut.GetTypeOfFieldNamed("Field1").Kind())
	assert.EqualValues(t, reflect.Float32, sut.GetTypeOfFieldNamed("Field2").Kind())
	assert.EqualValues(t, reflect.Bool, sut.GetTypeOfFieldNamed("Field3").Kind())
	assert.EqualValues(t, reflect.String, sut.GetTypeOfFieldNamed("Field4").Kind())
	// assert.EqualValues(t, reflect.Uint8, sut.GetTypeOfFieldNamed("Field5").Kind())
	assert.EqualValues(t, reflect.Uint8, sut.GetTypeOfFieldNamed("Field6").Kind())
	assert.EqualValues(t, reflect.Slice, sut.GetTypeOfFieldNamed("Fields7").Kind())
}
