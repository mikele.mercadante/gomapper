package reflectioninfo

import "reflect"

type ReflectionInfo interface {
	GetTypeOfFieldNamed(name string) reflect.Type
	GetFieldsName() []string
}

type reflectionInfo struct {
	fieldsType map[string]reflect.Type
}

func New(obj interface{}) reflectionInfo {

	reflectElement := reflect.ValueOf(obj).Elem()
	fields := make(map[string]reflect.Type)
	for i := 0; i < reflectElement.NumField(); i++ {
		fields[reflectElement.Type().Field(i).Name] = reflectElement.Type().Field(i).Type
	}

	return reflectionInfo{fieldsType: fields}
}

func (ri *reflectionInfo) GetTypeOfFieldNamed(name string) reflect.Type {
	return ri.fieldsType[name]
}

func (ri *reflectionInfo) GetFieldsName() []string {
	keys := reflect.ValueOf((*ri).fieldsType).MapKeys()
	keysName := make([]string, len(keys))
	for i, val := range keys {
		keysName[i] = val.String()
	}
	return keysName
}
